ManPagesTextFile
"ProsoReport" "jeanphilippegoldman" 20191104 0
<entry> "What is ProsoReport?"
<normal> "ProsoReport creates a global report on prosodic parameters in a sound file or a collection of sound files. The table is calculated from the Table of syllables @@LocalProsodicParameters@. ProsoReport documents temporal variables (proportion of articulated time, articulation rate, syllable mean duration, etc.), melodic features (f0 min and max, f0 dynamic, etc.) and accentuation (proportion of prominent syllables, etc.)."
<normal> "A ProsoReport can be calculated for one sound file or for a collection of sound files."
<entry> "How to use the script?"
<normal> "Open one or several Tables of syllables (FILE-NAME_syllsheet.txt) in the Praat Objects list: Open/Read from file..." 
<normal> "Select one or several Tables of syllables (syllsheet.txt files) to be processed."
<list_item> "\bu ##prosoreport name#: Insert a name for the ProsoReport Table."
<list_item> "\bu ##split 1 column#: Select a tier with a specific annotation (for example: prominent and non prominent syllables) that will be used to refine prosodic measures. By default, the %promauto tier is selected."
<list_item> "\bu ##split 1 categories#: List the labels present in the above mentionned tier. Prosodic measures will be refined according to those labels. By default: 0 = non prominent syllable; 1 = prominent syllable."
<list_item> "\bu ##split 2 column#: Select a tier with a specific annotation (for example: initial or final-word syllable) that will be used to refine prosodic measures. By default, the %if tier is selected with syllabic positions within the word."
<list_item> "\bu ##split 2 categories: List the labels present in the above mentionned tier. Prosodic measures will be refined according to those labels. By default: i = word-initial syllable; f = word-final syllable; ? = other syllabic position.#"
<list_item> "\bu ##mix split categories#: Check this box in case you want to mix the above mentionned categories (for example prominence and position)."
<list_item> "\bu ##do mean across tables#: Select this option if you create a ProsoReport from several tables of Syllables. The option adds a new colunm with mean values for each prosodic parameter."

<entry> "What do I get after this step is done?"
<normal> "A ProsoReport table is created in the list of Objects in Praat. Save it as a tab-separated file and modify the file extension into .txt. Then, you can easily open the table in a spreadsheet software. When opening the file, select Unicode (UTF-8) and tabulation as a separator."

<entry> "References"
<normal> "ProsoReport was initially developped by Jean-Philippe Goldman, Antoine Auchlin, Mathieu Avanzi and Anne Catherine Simon under the French name of Phonostylographe. It was then used for many studies about phonostylistics."
<list_item> "\bu Goldman, J.-P., Auchlin, A., Simon, A.C., Avanzi, M. 2007. ##Phonostylographe : un outil de description prosodique. Comparaison du style radiophonique et lu#. %%Nouveaux cahiers de linguistique française%, 28, p. 219-237."
<list_item> "\bu Hupin, B., Simon, A.C. 2009. ##Analyse phonostylistique du discours radiophonique. Expériences sur la mise en fonction professionnelle du phonostyle et sur le lien entre mélodicité et proximité du discours radiophonique#. %%Recherches en communication%, 28, p. 103-121."
