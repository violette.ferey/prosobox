ManPagesTextFile
"ProsoPromThresholds" "jeanphilippegoldman" 20191104 0
<entry> "The submenu ProsoPromThresholds"
<normal> "The submenu allows for modifying the acoustic thresholds used for prominence detection."

<entry> "How to fill in the form?"
<normal> "##Thresholds for P (prominence detection)#"
<normal> "A syllable is detected as prominent when one (or more) parameters is above the threshold. The submenu allows for modifying the (relative or absolute) values of the thresholds."
<list_item> "\bu ##Number of thresholds to be prom# Indicate the number of thresholds to be reached for a syllable to be detected as prominent (min = 1, max = 6). TO-CHECK WHAT IF GRADUAL STRATEGY IS SELECTED. By default, the number = 1."
<list_item> "\bu ##F0 threshold# : Indicate the relative pitch heigth (in semi-tones). By default, the threshold = 2"
<list_item> "\bu ##Dur threshold# : Indicate the relative duration. By default, the threshold = 2 (= twice as long)"
<list_item> "\bu ##Rise threshold# : Indicate the pitch movement (in semi-tones). By default, the threshold = 3.5"
<list_item> "\bu ##Pause threshold# : Indicate the pause length (after the target syllable) in milliseconds causing prominence detection. By default, the threshold = 300"
<list_item> "\bu ##Pause prev threshold# Indicate the duration of the pause before the target syllable (in milliseconds) causing prominence detection. By default, the threshold = 100"
<list_item> "\bu ##Intensity threshold# TO-CHECK By default, the threshold = 100"

<normal> "##Thresholds for B (final accent with falling f0)#"
<normal> "Right boundaries of prosodic units in French are often signalled with a falling intonation movement and a silent pause. The B (French %Bas, Low) detection allows for distinguishing P (prominent) syllables from B (final-utterance) syllables, if needed."
<list_item> "\bu ##Number of thresholds to be prom# Indicate the number of thresholds to be reached for a syllable to be detected as B (min = 1, max = 2). By default, the number = 1."
<list_item> "\bu ##F0 threshold# : Indicate pause duration (TO-CHECK SEC OR MILLISEC). By default, the threshold = 0.05"
<list_item> "\bu ##Pause threshold# : Indicate the relative duration. By default, the threshold = 2 (= twice as long)"

<normal> "##Thresholds for Z (filled pauses)#"
<normal> "Lengthening, when accompagnied by creaky voice or a light slope in FO, may characterize disfluent syllables also known as filled pauses. This is typically the case for %euh in French. The Z detection allows for distinguishing P (prominent) syllables from Z (filled pauses) syllables, if needed."
<list_item> "\bu ##Number of thresholds to be prom# Indicate the number of thresholds to be reached for a syllable to be detected as prominent (min = 1, max = 3). By default, the number = 1."
<list_item> "\bu ##F0 threshold# : Indicate the relative pitch height (in semi-tones). By default, the threshold = -5"
<list_item> "\bu ##Dur threshold# : Indicative the relative duration. By default, the threshold = 3 (= three times as long)"
<list_item> "\bu ##Cum threshold# : TO-CHECK. By default, the threshold = 0.6"

<normal> "Go back to @@ProsoProm@"