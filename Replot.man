ManPagesTextFile
"Replot" "jeanphilippegoldman" 20191104 0
<entry> "What is the Replot Enriched Prosogram function?"
<normal> "@@Prosogram@ is a tool that segments pitch into syllabic nuclei, stylizes intonation and produces a graphic (visual representation) of pitch, duration and intensity of segments. The ##Enriched Prosogram# adds to this graphic several local and global prosodic measures computed earlier in the ProsoBox set of tools. Local measures relates to syllabic relative duration and pitch as they contribute to prominence detection. Global measures relate to speech segments and describe speech rate, f0 register and prominence density from one speech segment to another."
<entry> "How to fill in the form?"
<normal> "The form of Enriched Prosogram is very similar to the one of Prosogram, so we refer to Prosogram User's Manual (Piet Mertens) for the similar sections: https://sites.google.com/site/prosogram/userguide\# control"
<normal> "We describe hereafter the additional sections as they appear in the ##Replot Settings# form:" 
<normal> "##Do plot blue green prom errors#: This graphic compares two tiers with prominent syllables (the %promauto tier with automatic detection and a %prommanu tier with manual detection) and displays the result of this comparison using colors:"
<list_item> "\bu A syllable that has been detected as prominent in both tiers (automatic and manual) is drawn in #red."
<list_item> "\bu A syllable that has been detected as non-prominent in both tiers is drawn in #black."
<list_item> "\bu A syllable that has been detected as prominent by the automatic detection only is drawn in #green."
<list_item> "\bu A syllable that has been detected as prominent by the human annotator only is drawn in #blue."
<list_item> "\bu A syllable that is non valid (disfluency, schwa, etc.) is drawn in #grey and is excluded from the comparison."  
<normal> "##Do plot syll parameters#"
<normal> "Prominent syllables are drawn in red while non prominent syllables are drawn in black. Each syllable comes with 2 or 3 measures related to prominence detection:" 
<list_item> "\bu The ##number printed in red# above the syllable indicates the relative f0 of the syllable nucleus (in semi-tones): it is positive if the syllable pitch is higher than the mean pitch of the surrounding syllables, and negative if it is not so."
<list_item> "\bu The ##number printed in green# below the syllable indicates the relative duration of the syllable: it is higher than 1.0 if the syllable is longer than the mean duration of the surrounding syllables, and smaller than 1.0 if it is not so."
<list_item> "\bu The ##number printed in blue# above the syllable, if any, represents the positive pitch movement within the syllable nucleus. It is expressed in semi-tones."
<list_item> "\bu An asterisk (star) next to a number indicates that the syllable has been detected as prominent because of that acoustic parameter."
<normal> "##Do plot speech segments parameters#"
<normal> "Prosodic parameters of the speech segments are displayed at the right boundary of each speech segment. The following 4 measures are displayed: number of syllables and pauses (p); speech or articulation rate; f0 mean (or range); proportion of prominent syllables."
<normal> "#Parameters:"
<list_item> "\bu ##ss tier#: Specify the name of the speech segment tier (the same as the one used for detecting prominent syllables with the script @@ProsoProm@"
<list_item> "\bu ##ss rate#: Choose the unit for reporting speech rate within the speech segment:" 
<list_item> "1. Syll mean duration (in s): Mean duration of syllables (in seconds)."
<list_item> "2. Articulation rate: number of articulated syllables / articulation time (in syllables per second)."
<list_item> "3. Speech rate: number of articulated syllables / total speech time including pauses (in syllables per second)."
<list_item> "\bu ##ss f0#: Choose the scale for expressing fundamental frequency measures:"
<list_item> "1. Minimal pitch - Mean pitch - Maximal pitch (in semi-tones or in Hz)." 
<list_item> "2. Mean pitch - Range (in semi-tones or in Hz)."
<list_item> "\bu ##ss f0# unit: Choose the unit for expressing pitch values:" 
<list_item> "1. Hz (Hertz)." 
<list_item> "2. ST (semi-tones relative to 1 Hz)."
<list_item> "\bu ##ss prom#: Choose the measure for prominent syllables ratio:" 
<list_item> "1. Percentage of prominent syllables per 100 syllables." 
<list_item> "2. 1/n = proportion of prominent syllables per syllables. 1/5 means that 1 out of 5 syllables is prominent."
<list_item> "3. nprom/nsyll = number of prominent syllables divided by the total number of syllables."
<entry> "What do I get after this step is completed?"
<normal> "Pictures accompagnied with measures per syllable and per speech segment, in various file formats (EMF, EPS)."
