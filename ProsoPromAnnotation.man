ManPagesTextFile
"ProsoPromAnnotation" "jeanphilippegoldman" 20191104 0
<entry> "The submenu ProsoPromAnnotation"
<normal> "The submenu allows for describing alternative annotation tiers before comparing them with the automatic detection of prominent syllables (tier %promauto)."

<entry> "How to fill in the form?"
<normal> "##Delivery#"
<normal> "The delivery tier contains annotation of local prosodic events like prominent syllables and disfluencies (like repetitions or filled pauses). A set of symbols useful for delivery annotation is available on the C-PROM website: https://sites.google.com/site/corpusprom/description#TOC-Inventaire-des-symboles-utilis-s-pour-l-annotation TO-CHECK URL"
<list_item> "\bu ##Delivery tier# Indicate the name of the tier where your annotation is encoded. The tier is in the TextGrid selected for the ProsoProm script."
<list_item> "\bu ##Prom label# : List (without space nor comma) the letters used for annotating prominent syllables. By default, we used: P, b and b (Ppb). Empty intervals (without any label) are considered as non prominent."
<list_item> "\bu ##Exclude label# : List the labels (and intervals) you want to exclude for the analysis. "
<list_item> "\bu ##Excluded intervals# : TO-CHECK Choose what to do with excluded intervals: ##Block relativization#, that is reduce the scope (window of analysis for the target syllable); ##Are just ignored# TO-CHECK; ##Are included in rel# TO-CHECK"
<list_item> "\bu ##Pause label# : List (without space nor comma) the symbols used to annotate silent pauses. For example: _ or *"
<list_item> "\bu ##Pause intervals# TO-CHECK Choose what to do with excluded intervals: ##Block relativization#, that is reduce the scope (window of analysis for the target syllable); ##Are just ignored# TO-CHECK; ##Are included in rel# TO-CHECK"
<list_item> "\bu ##Hesitation label# List (without space nor comma) the symbols used to annotate hesitations or disfluencies, for example: z or !"
<list_item> "\bu ##Hesitation intervals# TO-CHECK Choose what to do with excluded intervals: ##Block relativization#, that is reduce the scope (window of analysis for the target syllable); ##Are just ignored# TO-CHECK; ##Are included in rel# TO-CHECK"

<normal> "##External annotation#"
<normal> "TO-CHECK."
<list_item> "\bu ##if tier# Encode the name of the tier where lexical information can be found. TO-CHECK"

<normal> "Go back to @@ProsoProm@"